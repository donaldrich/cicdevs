---
title: Nuxt.Js
path: tree/master
source: site-gen/nuxt.js/Dockerfile
---

# donaldrich/nuxt.js

[![Docker Image Size (tag)](https://img.shields.io/docker/image-size/donaldrich/nuxt.js/latest?color=blue&label=size&logo=docker&style=flat-square)](https://hub.docker.com/r/donaldrich/nuxt.js/latest)

## Description

### Purpose

Container for generating Nuxt.js

### Features

- Nuxt.js

- Node

- Yarn

## Develop in Docker

### Command

```sh
docker pull donaldrich/nuxt.js:latest
docker run -it --rm \
--hostname=nuxt.js \
-v "$(pwd)":"/work" -w "/work" \
--entrypoint="/bin/zsh" \
--net="host" \
donaldrich/nuxt.js:latest
```

## Inspect Container

???+ info "Container Check"

    === "Validate Services"
        ```sh
        docker pull donaldrich/nuxt.js:latest && docker run -it --rm --entrypoint="tusk" donaldrich/nuxt.js:latest validate
        ```

    === "Check Versions"
        ```sh
        docker pull donaldrich/nuxt.js:latest && docker run -it --rm --entrypoint="tusk" donaldrich/nuxt.js:latest version
        ```

    === "Inspect Layers"
        ```sh
        docker pull donaldrich/nuxt.js:latest && docker run --rm -it -v /var/run/docker.sock:/var/run/docker.sock wagoodman/dive:latest donaldrich/nuxt.js:latest
        ```
    === "See Layer Info"

        ```sh
        docker pull donaldrich/nuxt.js:latest && docker history donaldrich/nuxt.js:latest
        ```
